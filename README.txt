
Description 
=============
This module allows payment in Ubercart through the Norwegian payment gate 
Netaxept, delivered by Nets. Currently only supports redirect payment type.


Installation
=============
1. Enable the module from the modules overview (admin/build/modules).
2. Go to Ubercart Payment methods configuration 
   (admin/store/settings/payment/edit/methods) and make sure you set your 
   Merchant ID and secret token in the Nets Netaxept settings. 
3. If you want to test the service first, set URL to Nets server to 
   https://epayment-test.bbs.no.
4. Make sure your currency is set correctly in Store settings -> Format 
   settings (admin/store/settings/store/edit/format).
